<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {
	public function __construct()
	{	
		parent::__construct();
		$this->load->library('cart');
		$this->load->library('session');
		$this->load->model('keranjang_model');
		$this->API = "http://localhost/Tugas3_Rekweb_Senin09_163040151_Robi";
	}
	public function index()
		{
			$data['produk'] = json_decode($this->curl->simple_get($this->API .'/tbl_produk/'));
			$data['kategori'] = json_decode($this->curl->simple_get($this->API .'/tbl_kategori/'));

			$this->load->view('themes/header',$data);
			$this->load->view('shopping/list_produk',$data);
			$this->load->view('themes/footer');
		}
	public function tentang()
		{
			$data['kategori'] = json_decode($this->curl->simple_get($this->API .'/tbl_kategori/'));
			
			$this->load->view('themes/header',$data);
			$this->load->view('pages/tentang',$data);
			$this->load->view('themes/footer');
		}	
	public function cara_bayar()
		{
			$data['kategori'] = json_decode($this->curl->simple_get($this->API .'/tbl_kategori/'));
			$this->load->view('themes/header',$data);
			$this->load->view('pages/cara_bayar',$data);
			$this->load->view('themes/footer');
		}	
	public function admin()
		{
			$data['produk'] = json_decode($this->curl->simple_get($this->API .'/tbl_produk/'));
			$kategori=($this->uri->segment(3))?$this->uri->segment(3):0;
			$data['kategori'] = json_decode($this->curl->simple_get($this->API .'/tbl_kategori/'));
			$data['produk'] = json_decode($this->curl->simple_get($this->API .'/tbl_produk/'));
			$this->load->view('themes/header-admin',$data);
			$this->load->view('admin/index',$data);
			$this->load->view('themes/footer');
		}	
	public function tambah()
		{
			// $data['produk'] = $this->keranjang_model->get_produk_all();
			$data['kategori'] = json_decode($this->curl->simple_get($this->API .'/tbl_kategori/'));
			$this->load->view('themes/header-admin',$data);
			$this->load->view('admin/insert',$data);
			$this->load->view('themes/footer');
		}
	public function orderan()
		{
			$data['produk'] = json_decode($this->curl->simple_get($this->API .'/tbl_pelanggan/'));
			$data['kategori'] = json_decode($this->curl->simple_get($this->API .'/tbl_kategori/'));
			// $kategori=($this->uri->segment(3))?$this->uri->segment(3):0;
			// $data['produk'] = $this->keranjang_model->get_produk_kategori($kategori);
			$this->load->view('themes/header-admin',$data);
			$this->load->view('admin/orderan',$data);
			$this->load->view('themes/footer');
		}
	public function _uploadImage()
		{
			$config['upload_path']  = 'C:\xampp\htdocs\toko\assets\images';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['file_name'] = $this->input->post('id');
			$config['overwrite'] = true;
			$config['max_size'] = 5120;

			$this->load->library('upload', $config);
			if ($this->upload->do_upload('gambar')) {
				return $this->upload->data("file_name");
			}
			return "default.jpg";
		}
	public function add() 
	{

		$id = $this->input->post('id');
		$nama_produk = $this->input->post('nama_produk');
		$harga = $this->input->post('harga');
		$stok = $this->input->post('stok');
		$kategori = $this->input->post('kategori');
		$deskripsi = $this->input->post('deskripsi');
		$gambar = $this->_uploadImage();
		if (isset($_POST['submit'])) {
			$prk = array(
				'id_produk' => $id,
				'nama_produk' => $nama_produk,
				'deskripsi' => $deskripsi,
				'harga' => $harga,
				'stok' => $stok,
				'gambar' => $gambar,
				'kategori' => $kategori
			);
			$this->curl->simple_post($this->API . '/tbl_produk/', $data, array(CURLOPT_BUFFERSIZE => 10));
			$data['produk'] = json_decode($this->curl->simple_get($this->API .'/tbl_produk/'));
			$data['kategori'] = json_decode($this->curl->simple_get($this->API .'/tbl_kategori/'));
			echo json_encode(array("status" => TRUE));
			$this->load->view('themes/header-admin',$data);
			$this->load->view('admin/index',$data);
			$this->load->view('themes/footer');
		}
	}
	public function hapus($id)
	{
		json_decode($this->curl->simple_delete($this->API . '/tbl_produk/', array('id' =>$id), array(CURLOPT_BUFFERSIZE => 10)));
		echo json_encode(array("status" => TRUE));
		$this->admin();
	}
	public function update($id)
	{
		$data['prk'] = json_decode($this->curl->simple_get($this->API . '/tbl_produk', $params));
		$data['kategori'] = json_decode($this->curl->simple_get($this->API . '/Tabel_kategori', $params));
		$this->load->view('themes/header-admin',$data);
		$this->load->view('admin/update',$data);
		$this->load->view('themes/footer');
	}
	public function edit() 
	{
		if (isset($_POST['submit'])) {
		$id = $this->input->post('id');
		$nama_produk = $this->input->post('nama_produk');
		$harga = $this->input->post('harga');
		$stok = $this->input->post('stok');
		// $kategori = $this->input->post('kategori');
		$deskripsi = $this->input->post('deskripsi');
		
		if (!empty($_FILES["gambar"]["name"])) {
			$gambar = $this->_uploadImage();
		} else{
			$gambar = $this->input->post('image');
		}
		if ($_POST["kategori"] == '0') {
			$kategori = $this->input->post('ktgr');
		} else{
			$kategori = $this->input->post('kategori');
		}
		
			$prk = array(
				'id_produk' => $id,
				'nama_produk' => $nama_produk,
				'deskripsi' => $deskripsi,
				'harga' => $harga,
				'stok' => $stok,
				'gambar' => $gambar,
				'kategori' => $kategori
			);
			// $this->keranjang_model->insertProduk($prk);
			$this->curl->simple_put($this->API . '/tbl_produk/', $data, array(CURLOPT_BUFFERSIZE => 10));
			echo json_encode(array("status" => TRUE));	
			$this->admin();
		}
	}
	public function login()
	{
		$this->load->view('themes/header-login');
		$this->load->view('admin/login');
		// $this->load->view('themes/footer');
	}

	public function logout()
	{
		$this->login();
		// $this->load->view('themes/footer');
	}

	public function signin(){
		if (isset($_POST['submit'])) {
			$username = $this->input->post('username');
			$password = $this->input->post('password');


			if ($username == "levin" && $password == "levin@98") {
				$this->admin();
			} else {
				$this->session->set_flashdata('gagal','<small style="color:red;">Periksa kembali account</small>');
				$this->login();
			}
		}
	}
}
