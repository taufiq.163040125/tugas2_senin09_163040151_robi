<h2>Daftar Buku</h2>
<?php
  foreach ($produk as $key) : 
?>
            <div class="col-lg-4 col-md-6 mb-4">
              <div class="kotak">
              <form method="post" action="<?php echo base_url();?>shopping/tambah" method="post" accept-charset="utf-8">
                <a href="#"><img class="img-thumbnail" src="<?php echo base_url() . 'assets/images/'.$key->gambar ?>" style="width: 200px; height: 180px;"/></a> 
                <div class="card-body">
                  <h4 class="card-title">
                    <a href="#"><?= $key->nama_produk ?></a>
                  </h4>
                  <h5>Rp. <?php echo number_format($key->harga ,0,",",".");?></h5>
                </div>
                <div class="card-footer">
                  <a href="<?php echo base_url();?>shopping/detail_produk/<?= $key->id_produk ?>" class="btn btn-sm btn-default"><i class="glyphicon glyphicon-search"></i> Detail</a> 
                  
                  <input type="hidden" name="id" value="<?= $key->id_produk ?>" />
                  <input type="hidden" name="nama" value="<?=$key->nama_produk ?>" />
                  <input type="hidden" name="harga" value="<?=  $key->harga ?>" />
                  <input type="hidden" name="gambar" value="<?= $key->gambar ?>" />
                  <input type="hidden" name="qty" value="1" />
                  <button type="submit" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-shopping-cart"></i> Beli</button>
                </div>
                </form>
              </div>
            </div>
<?php
  endforeach
?>