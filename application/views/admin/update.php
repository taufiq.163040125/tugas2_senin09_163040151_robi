<div style=" border: solid; border-color: grey; border-radius:20px; width: 400px; margin-left: 300px; background-color: grey; ">
<h3 style="color: white; margin-left: 100px; class="text-center">Ubah Daftar Buku</h3>
<form action="<?php echo site_url('page/edit') ?>" method="post" enctype="multipart/form-data">
<?php foreach($prk as $prk): ?>
  <div class="form-group">
    <!-- <label for="exampleInputEmail1">ID Produk</label> -->
    <input type="hidden" class="form-control-filetrol" id="exampleInputId"  placeholder="Masukan ID Produk" name="id" value="<?php echo $prk->id_produk ?>">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1" style="margin-left: 160px; color: white;">Nama Buku</label>
    <input type="text" class="form-control" id="exampleInputNama"  placeholder="Masukan Nama Produk" name="nama_produk" value="<?php echo $prk->nama_produk ?> " style="width: 250px; margin-left: 90px; ">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1" style="margin-left: 170px; color: white">Deskripsi</label>
    <input style="width: 250px; height: 100px; margin-left: 90px;" type="text" class="form-control" id="exampleInputDeskripsi" placeholder="Masukan Deskripsi" name="deskripsi" value="<?php echo $prk->deskripsi ?>" >
  </div>
  <div class="form-group form-check">
    <label for="exampleInputEmail1" style="margin-left: 180px; color: white">Harga</label>
    <input type="number" class="form-control" id="exampleInputHarga" placeholder="Masukan Harga" name="harga" value="<?php echo $prk->harga ?>" style="width: 250px; margin-left: 90px; ">
  </div>
  <div class="form-group form-check">
    <label for="exampleInputEmail1" style="margin-left: 160px; color: white">Jumlah Buku</label>
    <input type="number" class="form-control" id="exampleInputStok" placeholder="Masukan jumlah stok barang" name="stok" value="<?php echo $prk->stok ?>" style="width: 250px; margin-left: 90px; ">
  </div>
  <div class="form-group">
    <label for="exampleFormControlFile1" style="margin-left: 180px; color: white;">Cover</label>
    <input type="hidden" name="image" value="<?php echo $prk->gambar ?>">
    <input type="file" class="form-control-file" id="exampleFormGambar" name="gambar" style="width: 250px; margin-left: 90px; ">
    <small style="margin-left: 90px;">max Size 5MB</small>
  </div>
  <div class="form-group form-check">
    <label class="form-check-label" for="exampleCheck1" style="margin-left: 140px; color: white">Kategori </label>&nbsp;
    <input type="hidden" name="ktgr" value="<?php echo $prk->kategori ?>" style=" margin-left: 90px; ">
    <select name="kategori">
  <option value="0" selected>-Pilih Kategori-</option>
  <option value="1">Horor</option>
  <option value="2">Romantis</option>
  <option value="3">Tutorial</option>
</select>
  </div>
  
  <button type="submit" class="btn btn-success" name="submit" style="margin-left: 160px;">Ubah</button>&nbsp;
<?php endforeach ?>
</form>
</div>