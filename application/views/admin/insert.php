<div style=" border: solid; border-color: grey; border-radius:20px; width: 400px; margin-left: 300px; background-color: grey; ">
<h3 style="color: white;" class="text-center">Tambah Daftar Buku</h3>

<form action="add" method="post" enctype="multipart/form-data ">
  <div class="form-group">
    <label for="exampleInputEmail1" style="margin-left: 170px; color: white;">ID Buku</label>
    <input type="text" class="form-control" id="exampleInputId"  placeholder="Masukan ID Produk" name="id" style="width: 250px; margin-left: 90px;">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1" style="margin-left: 160px; color: white;">Nama Buku</label>
    <input type="text" class="form-control" id="exampleInputNama"  placeholder="Masukan Nama Produk" name="nama_produk" style="width: 250px; margin-left: 90px;">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1" style="margin-left: 170px; color: white;">Deskripsi</label>
    <input style="width: 250px; height: 100px; margin-left: 90px;" type="text" class="form-control" id="exampleInputDeskripsi" placeholder="Masukan Deskripsi" name="deskripsi">
  </div>
  <div class="form-group form-check">
    <label for="exampleInputEmail1" style="margin-left: 170px; color: white;">Harga</label>
    <input type="number" class="form-control" id="exampleInputHarga" placeholder="Masukan Harga" name="harga" style="width: 250px; margin-left: 90px;">
  </div>
  <div class="form-group form-check">
    <label for="exampleInputEmail1" style="margin-left: 150px; color: white;">Jumlah Buku</label>
    <input type="number" class="form-control" id="exampleInputStok" placeholder="Masukan jumlah stok barang" name="stok" style="width: 250px; margin-left: 90px;">
  </div>
  <div class="form-group">
    <label for="exampleFormControlFile1" style="margin-left: 170px; color: white;">Cover</label>
    <input type="file" class="form-control-file" id="exampleFormGambar" name="gambar" style=" margin-left: 90px;">
    <small style="margin-left: 90px">Max Size 5MB</small>
  </div>
  <div class="form-group form-check" >
    <label class="form-check-label" for="exampleCheck1" style="margin-left: 150px; color: white;" >Kategori </label>&nbsp;
    <select name="kategori">
  <option value="1">Horor</option>
  <option value="2">Romantis</option>
  <option value="3">Tutorial</option>
</select>
  </div>
  
  <button type="submit" class="btn btn-success" name="submit" style="margin-left: 150px;">Tambah</button> &nbsp;
  <div><br></div>
</form>
