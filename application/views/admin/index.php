<h2 style="color: black;">Daftar Buku</h2>
<br>
<table class="table">
<tr id= "main_heading">

<th style="color: white; background-color: black;" class="text-center" width="10%">ID Buku</th>
<th style="color: white; background-color: black;" class="text-center" width="10%">Cover</th>
<th style="color: white; background-color: black;" class="text-center" width="15%">Nama Buku</th>
<th style="color: white; background-color: black;" class="text-center" width="10%">Harga</th>
<th style="color: white; background-color: black;" class="text-center" width="10%">Jumlah Buku</th>
<th style="color: white; background-color: black;" class="text-center" width="10%">Kategori</th>
<th style="color: white; background-color: black;" class="text-center" width="15%">Aksi</th>
</tr>
<?php
// Create form and send all values in "shopping/update_cart" function.
$grand_total = 0;
$i = 1;

foreach ($produk as $item):
?>

<tr>

<td style="background-color: grey; color: white;" class="text-center"><?php echo $item->id_produk ?></td>
<td style="background-color: grey; color: white;" ><img class="img-responsive" src="<?php echo base_url() . 'assets/images/'.$item->gambar ?>"/></td>
<td style="background-color: grey; color: white;"><?php echo $item->nama_produk ?></td>

<td style="background-color: grey; color: white;" class="text-center"><h5>Rp.<?php echo $item->harga; ?></h5></td>
<td style="background-color: grey; color: white;" class="text-center"><?php echo $item->stok ?></td>
<td style="background-color: grey; color: white;" class="text-center"><?= $item->kategori ?></td>
<td style="background-color: grey; color: white;" class="text-center">
	<a href="<?php echo site_url('page/update/'.$item->id_produk) ?>" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-edit"></i></a>
	<a href="<?php echo site_url('page/hapus/'.$item->id_produk) ?>" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-trash"></i></a>
	
</td>
<?php endforeach; ?>
</tr>

</table>