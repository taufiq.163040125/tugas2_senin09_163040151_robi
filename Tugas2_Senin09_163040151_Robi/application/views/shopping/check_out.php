<h2>Konfirmasi Check Out</h2>
<h3>Konfirmasi Produk</h3>
<div class="kotak2">
<?php
$grand_total = 0;
if ($cart = $this->cart->contents())
	{
		foreach ($cart as $item)
			{
				$grand_total = $grand_total + $item['subtotal'];
			}
		// echo "<h4>Total Belanja: Rp.".number_format($grand_total,0,",",".")."</h4>";	
?>

<table class="table">
<tr id= "main_heading">
<td width="2%">No</td>
<td width="10%">Gambar</td>
<td width="33%">Item</td>
<td width="17%">Harga</td>
<td width="8%">Qty</td>
<td width="20%">Jumlah</td>
</tr>
<?php
// Create form and send all values in "shopping/update_cart" function.
$grand_total = 0;
$i = 1;

foreach ($cart as $item):
$grand_total = $grand_total + $item['subtotal'];
?>
<input type="hidden" name="cart[<?php echo $item['id'];?>][id]" value="<?php echo $item['id'];?>" />
<input type="hidden" name="cart[<?php echo $item['id'];?>][rowid]" value="<?php echo $item['rowid'];?>" />
<input type="hidden" name="cart[<?php echo $item['id'];?>][name]" value="<?php echo $item['name'];?>" />
<input type="hidden" name="cart[<?php echo $item['id'];?>][price]" value="<?php echo $item['price'];?>" />
<input type="hidden" name="cart[<?php echo $item['id'];?>][gambar]" value="<?php echo $item['gambar'];?>" />
<input type="hidden" name="cart[<?php echo $item['id'];?>][qty]" value="<?php echo $item['qty'];?>" />
<tr>
<td><?php echo $i++; ?></td>
<td><img class="img-responsive" src="<?php echo base_url() . 'assets/images/'.$item['gambar']; ?>" style="height: 200px; width: 200px;"/></td>
<td><?php echo $item['name']; ?></td>
<td><?php echo number_format($item['price'], 0,",","."); ?></td>
<td>&nbsp;<?php echo $item['qty']; ?></td>
<td><?php echo number_format($item['subtotal'], 0,",",".") ?></td>
<?php endforeach; ?>
</tr>
<tr>
<td colspan="3"><h4><b>Total Pembayaran: Rp <?php echo number_format($grand_total, 0,",","."); ?></b></h4></td>
</tr>

</table>
<h3>Konfirmasi Identitas</h3><br>
<form class="form-horizontal" action="<?php echo base_url()?>shopping/proses_order" method="post" name="frmCO" id="frmCO">
        <div class="form-group  has-success has-feedback">
            <label class="control-label col-xs-1" for="inputEmail">Email:</label>
            <div class="col-xs-10">
                <input type="email" class="form-control" name="email" id="email" placeholder="Email">
            </div>
        </div>
        <div class="form-group  has-success has-feedback">
            <label class="control-label col-xs-1" for="firstName">Nama:</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Lengkap">
            </div>
        </div>
        <div class="form-group  has-success has-feedback">
            <label class="control-label col-xs-1" for="lastName">Alamat:</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Alamat">
            </div>
        </div>
        <div class="form-group  has-success has-feedback">
            <label class="control-label col-xs-1" for="phoneNumber">Telp:</label>
            <div class="col-xs-10">
                <input type="tel" class="form-control" name="telp" id="telp" placeholder="No Telp">
            </div>
        </div>
        
        <div class="form-group  has-success has-feedback">
            <div class="col-xs-offset-2 col-xs-10">
                <button type="submit" class="btn btn-success">Proses Order</button>
            </div>
        </div>
    </form>
    <?php
	}
	else
		{
			echo "<h5>Shopping Cart masih kosong</h5>";	
		}
	?>
</div>