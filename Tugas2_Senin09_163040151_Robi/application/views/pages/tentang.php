<h2>Tentang Levin Books Store</h2>
<h3></h3>
<p>		Levin Books Store adalah anak perusahaan Kompas Gramedia yang menyediakan jaringan toko buku dengan nama Toko Buku Levin Books Store di beberapa kota di Indonesia dan Malaysia. Perusahaan ini didirikan pada tanggal 24 Januari 1988 dengan diawali dari satu toko buku kecil berukuran 25m² di daerah Bandung dan sampai tahun 2009 telah berkembang menjadi lebih dari 50 toko yang tersebar di seluruh Indonesia.</p>
<p>		Perusahaan ini bekerja sama dengan penerbit-penerbit buku baik dalam maupun luar negeri. Dari kelompok usahanya sendiri, pemasok ke Toko Buku Levin Books Store antara lain adalah Gramedia Pustaka Utama, Elex Media Komputindo, Gramedia Widya Sarana, Bhuana Ilmu Populer, dan Gramedia Majalah, sementara dari luar negeri misalnya Prentice Hall, McGraw Hill, Addison Wesley, dll.</p>

